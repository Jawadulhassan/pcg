
(function( $ ) {
  //equal height fuctions for elements
  equalheight = function(container){
"use strict";
	var currentTallest = 0,
			 currentRowStart = 0,
			 currentDiv = 0,
			 rowDivs = new Array(),
			 $el,
			 topPosition = 0;
	 $(container).each(function() {

		 $el = $(this);

		 $($el).height('auto')
		 window.topPostion = $el.position().top;

		 if (currentRowStart != topPostion) {
			 for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				 rowDivs[currentDiv].height(currentTallest);
			 }
			 rowDivs.length = 0; // empty the array
			 currentRowStart = topPostion;
			 currentTallest = $el.height();
			 rowDivs.push($el);
		 } else {
			 rowDivs.push($el);
			 currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
		}
		 for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			 rowDivs[currentDiv].height(currentTallest);
		 }
	 });
	}
  function contentHeight(){
    //min-height adjustment for page
    var winHt=$(window).outerHeight();
    var headerHt=$('.js-header').outerHeight();
    var footerHt=$('.js-footer').outerHeight();
    var mh=winHt-headerHt-footerHt;
    $('.main-contents').css("min-height", mh);
  }

  function requirtmentFloat(){
      if($('.recruitment-container').length){
        var el = $('.floating-sec');
        var stickyTop = $('.recruitment-container').offset().top - 122; // returns number
        var stickyHeight = $('.floating-sec').height();
        var limit = $('.footer_wraper').offset().top - stickyHeight - 43;
        if($(window).width()>1199){
          var topSpace = 120,
          headerSpace = $('.js-header').outerHeight();
        }else{
          var stickyTop = $('.recruitment-container').offset().top;
          var topSpace = 0,
          headerSpace = 0;
        }
        var windowTop = $(window).scrollTop(); // returns number
        // console.log()
        if (stickyTop + topSpace - headerSpace  < windowTop){
           el.css({ position: 'fixed', top: topSpace+'px' });
            
           $('.primery-img').css('opacity')
        }
        else {
           el.css('position','static');
        }
        // console.log($('.header').height()+$('.floating-sec').outerHeight('true')+$('.footer').outerHeight('true') >= $(window).height());
        if($(window).height()>1000){
        if(!el.hasClass('completed')){
          if($(window).scrollTop() + $(window).height() >= $(document).height()){
                el.css({'margin-top':(  $('.footer_wraper').offset().top - el.offset().top - $('.footer-inner-sec').outerHeight() + $('.header').outerHeight() + $('.footer_wrapper hr').outerHeight()) +'px'});
                el.addClass('completed');
          }
        }else{
                el.removeClass('completed').css({'margin-top':'64px'});
          }
        }else{
          if (limit-topSpace < windowTop) {
          var diff = limit - windowTop;
            el.addClass('completed').css({top: diff});
          }
        }
        
      }
    }
  // limit text in headings
  $(".event-inner-sec h3").each(function(){
    if($(this).text().length > 35){
      $(this).text($(this).text().substr(0,35)+'...');
    }
  });


  $(window).on( 'load resize' , function(){
    contentHeight();
    equalheight($('.events .event .event-inner-sec'));
  });

  $(window).on( 'load resize scroll' , function(){
    requirtmentFloat();
     var hdWidth=$('.bnr_hdng').outerWidth();
     console.log(hdWidth);
    $('.bnr-sub-heading').css('width', "calc(100% - " + (hdWidth - 57) +"px)");
    $(".primery-img").css("opacity", 1 - ($(window).scrollTop() - 300) / 250);
    $(".secondary-img").css("opacity", 0 + ($(window).scrollTop() - 300) / 250);
    // if($('.further-details').length){
    // var btnOffset=$('.further-details').offset().top
    // if($(window).scrollTop() > btnOffset - 800){
    //   $('.floating-sec').addClass('nextPos');
    // }else{
    //   $('.floating-sec').removeClass('nextPos');
    // }
    // }

  });



  //menu
  $('.lines-button').on('click',function(){

    $(this).toggleClass('close');
    $('html').toggleClass('menu-active');
    if ($('.lines-button').hasClass('close')){
      //min-height adjustment for page
      var winHt=$(window).outerHeight();
      var headerHt=$('.js-header').outerHeight();
      var mh=winHt-headerHt;
      $('.main-contents').css("height", mh);
    }else{
      $('.main-contents').css("height", "auto");
    }


  });

  $('.main-nav > ul > li.has-nav > a').on('click', function(x){
    if($(window).width() < 1200 ){
      x.preventDefault();
      $(this).next('.sub-menu').slideToggle();
    }
  });

  //recrutement page file buttons
  $('.file-btn-container input[type="file"]').each(function(){
    var txt=$(this).parents('.row').find('p').html();
    $(this).attr('data-placeholder', txt);
  });
  $('.detail-link, .js-trigger').on('click', function(x){
    x.preventDefault();
    $('body').addClass('form-active');
  });
  $('.recruitment-form .popup-closer').on('click', function(x){
    x.preventDefault();
    $('body').removeClass('form-active');
  });
  //recrutement page file upload buttons
  $('.file-btn-container input[type="file"]').on('change', function(){
    var val=$(this).val();
    var txt=$(this).data('placeholder');
    if(val.length >0){
      $(this).parents('.row').find('p').html(val);
    }else{
      $(this).parents('.row').find('p').html(txt);
    }

  });
  //footer evenements
  $.simpleTicker($('.ticker'),{
    'effectType':'roll',
    speed : 1000,
    delay : 5000,
    height :'auto'
  });
//   $('.event-nav').newsTicker({
//     row_height: 86,
//     max_rows: 1,
//     speed: 600,
//     direction: 'up',
//     duration: 4000,
//     autostart: 1,
//     pauseOnHover: 0
// });
  $('.menu-close-layer, .nav-container ul li  a').on('click',function(){

    $('.lines-button').removeClass('close');
    $('html').removeClass('menu-active');
    $('.main-contents').css("height", "auto");

  });

// tabs
  var mission_bnrs = {
    "audit":"banner_img_9.jpg",
    "road_map": "banner_img_8.jpg",
    "mission_benchmark": "banner_img_7.jpg",
    "mission": "banner_img_6.jpg",
    "specifications": "banner_img_5.jpg",
  };
  $('.wpb_tabs a').click(function(){
    var bnr = $(this).attr('href').replace('#', '');
    $(this).addClass('active').siblings().removeClass('active');
    $('.missions .wpb_pg_bnr').css('background-image', "url(img/"+mission_bnrs[bnr]+")");
    console.log(mission_bnrs[bnr]);
  });
  if($('.video-container').length>0){
    $('.video-container').on("init.slick", function (event, slick) {
      $(".video-container [data-slick-index=\"0\"]").find('video').get(0).play();
    });
    $('.video-container').slick({
      dots: true,
      infinite: true,
      speed: 2000,
      autoplay: true,
      fade: true,
      autoplaySpeed: 10000,
      pauseOnHover: false,
      pauseOnFocus: false,
      adaptiveHeight: true,
      arrows: false,
      useCSS: false,
      useTransform: false
    });
    $('.video-container').on('afterChange', function(event, slick, currentSlide, nextSlide){
        $('.video-container video').each(function(){
          $(this).get(0).pause();
        });
      $(".video-container [data-slick-index="+currentSlide+"]").find('video').get(0).play();
    });
  }

//  contact form

  $('#cntct_form').submit(function(e){
    e.preventDefault();
    $(this).addClass('submitted');

    setTimeout(function(){
      $('#cntct_form').removeClass('submitted');
      $('body').removeClass('form-active')
    }, 3000);

  });


// parallex balls contact page

  function scrollball() {
    $(document).scroll(function(){
     var scrollPos = $(this).scrollTop();
     $('.ball_1').css({
       'top' : (scrollPos/3)+'px',
     });
     $('.ball_2').css({
       'top' : ((scrollPos/3)+407)+'px',
       'opacity' : 1-(scrollPos/710)
     });
     $('.ball_3').css({
       'bottom' : ((scrollPos/6)-127)+'px',
     });
     $('.ball_4').css({
       'bottom' : (scrollPos/2)+'px',
       'opacity' : 1-(scrollPos/610)
     });
     $('.ball_5').css({
       'bottom' : ((scrollPos/5)+263)+'px',
     });
    });
  }

  // $('.more-btn').click(function(){
  //   var mission_page = $(this).data('page');
  //   localStorage.setItem('mission_page', mission_page)
  // });

  // $('.wpb_tabs > a[href="#'+localStorage.getItem('mission_page')+'"]').addClass('active');
  // $('.tab-content > #'+localStorage.getItem('mission_page')).addClass('active in').siblings().removeClass('active in');


  // console.log('.tab-content > #'+localStorage.getItem('mission_page'));

  scrollball();

//animations for sections and elements
  var $window = $(window),
      win_height_padded = $window.height() * 1.1,
      isTouch = Modernizr.touch;

  if (isTouch) {
      $('.revealOnScroll').addClass('animated');
  }

  $window.on('scroll', revealOnScroll);

  function revealOnScroll() {
      var scrolled = $window.scrollTop(),
          win_height_padded = $window.height() * 1.6;

      // Showed...
      $(".revealOnScroll:not(.animated)").each(function() {
          var $this = $(this),
              offsetTop = $this.offset().top;

          if (scrolled + win_height_padded > offsetTop) {
              if ($this.data('timeout')) {
                  window.setTimeout(function() {
                      $this.addClass('animated ' + $this.data('animation'));
                  }, parseInt($this.data('timeout'), 10));
              } else {
                  $this.addClass('animated ' + $this.data('animation'));
              }
          }
      });
      // Hidden...
      $(".revealOnScroll.animated").each(function(index) {
          var $this = $(this),
              offsetTop = $this.offset().top;
          if (scrolled + win_height_padded < offsetTop) {
              $(this).removeClass('animated fadeInUp flipInX lightSpeedIn');
          }
      });
  }
  revealOnScroll();

})(jQuery);
